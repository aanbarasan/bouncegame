package org.bouncer.Bouncing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class BouncingBall {

	public static final int ballsize = AppConfiguration.BALL_SIZE;
	public double xpos = 0, ypos = 0;
	public double direction = Math.random() % Math.PI + Math.PI;
	Image ballImage;
	public int[] directionchangelock = new int[3];

	public BouncingBall() {
		directionchangelock = new int[] { -1, -1, -1 };
	}

	public void initPosition(double ix, double iw, double iy) {
		xpos = ix + iw / 2;
		ypos = iy - ballsize;
	}

	public void drawball(Graphics g) {
		g.setColor(Color.red);
		g.fillOval((int) (xpos - ballsize), (int) (ypos - ballsize), 2 * ballsize, 2 * ballsize);
	}

	public void move() {
		xpos = xpos + (5 * Math.sin(direction));
		ypos = ypos + (5 * Math.cos(direction));
	}

	public boolean changedirection(int direc_i, int direc_j, int direc_k) {

		if (direc_i == directionchangelock[0] && direc_j == directionchangelock[1]
				&& direc_k == directionchangelock[2]) {
		} else {
			if (direc_k % 2 == 0) {
				if (direction % Math.PI < 180) {
					direction = Math.PI - direction;
				} else {
					direction = 3 * Math.PI - direction;
				}
			} else {
				direction = 2 * Math.PI - direction;
			}

			directionchangelock = new int[] { -1, -1, -1 };
		}

		if (direc_i != 1000) {
			return true;
		}
		return false;
	}
}
