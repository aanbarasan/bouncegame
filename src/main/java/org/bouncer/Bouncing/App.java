package org.bouncer.Bouncing;

import javax.swing.JOptionPane;
import java.util.Date;

public class App {

    private static BouncerFrame bouncerFrame;
    private static Integer GameLevel = 1;
    private static int score = 0;
    private static Date gameDateTime = new Date();

    public static void main(String[] args) {
        startGame();
    }

    private static void startGame() {
        try {
            bouncerFrame = new BouncerFrame(GameLevel, score, gameDateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void closeGame() {
        try {
            bouncerFrame.distroyComponent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void nexLevelMove() {
        try {
            GameLevel = GameLevel + 1;
            score = bouncerFrame.getScore();
            bouncerFrame.distroyComponent();
            if (GameLevel > 5) {
                JOptionPane.showConfirmDialog(null, "Thanks for playing", "Your high score is: " + score, JOptionPane.DEFAULT_OPTION);
				closeGame();
            } else {
                bouncerFrame = new BouncerFrame(GameLevel, score, gameDateTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void reStartGame() {
        try {
            GameLevel = 1;
            score = 0;
            gameDateTime = new Date();
            bouncerFrame.distroyComponent();
            bouncerFrame = new BouncerFrame(GameLevel, score, gameDateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void gameEnd() {
        bouncerFrame.stopThread();
        int result = JOptionPane.showConfirmDialog(null, "Restart the game?", "Game over", JOptionPane.OK_CANCEL_OPTION);
        System.out.println(result);
        if (result == 0) {
            reStartGame();
        } else {
            closeGame();
        }
    }
}
