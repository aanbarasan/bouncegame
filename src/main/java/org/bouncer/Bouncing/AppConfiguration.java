package org.bouncer.Bouncing;

import java.util.HashMap;

public class AppConfiguration {
	
	public static final int BALL_SIZE = 10;

	public static final int GAME_SPEED_NEGATIVE = 50;

	public static HashMap<Integer, int[]> GAME_LEVEL_WALL_MAP = new HashMap<Integer, int[]>(){{
		put(1, new int[]{10, 2});
		put(2, new int[]{10, 3});
		put(3, new int[]{10, 4});
		put(4, new int[]{10, 5});
		put(5, new int[]{10, 6});
	}};

}
