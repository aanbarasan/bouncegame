package org.bouncer.Bouncing;

import java.awt.*;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class BouncerPanel extends JPanel {

    int gameScore = 0;
    Thread thread;
    ColorBox colorBox;
    Date gameDateTimeValue;

    public BouncerPanel(ColorBox col, Date gameDateTime) {
        colorBox = col;
        this.gameDateTimeValue = gameDateTime;
        final JLabel gameLevelLabel = new JLabel("Level: " + colorBox.gameLevel);
        gameLevelLabel.setFont(new Font("Verdana", Font.BOLD, 20));
        gameLevelLabel.setBorder(new CompoundBorder(gameLevelLabel.getBorder(), new EmptyBorder(10,20,10,10)));
        final JLabel scoreLabel = new JLabel(getScoreText());
		scoreLabel.setFont(new Font("Verdana", Font.BOLD, 20));
        scoreLabel.setBorder(new CompoundBorder(scoreLabel.getBorder(), new EmptyBorder(10,20,10,10)));
        final JLabel timerLabel = new JLabel(getTotalTime());
		timerLabel.setFont(new Font("Verdana", Font.BOLD, 20));
		timerLabel.setBorder(new CompoundBorder(timerLabel.getBorder(), new EmptyBorder(10,20,10,10)));
		add(gameLevelLabel);
		add(scoreLabel);
		add(timerLabel);
        setBackground(new Color(137, 196, 244));

        thread = new Thread(new Thread() {
            public void run() {
                while (colorBox.runEndless) {
                    try {
                        colorBox.rearrangeBorder();
                        colorBox.moveBall();
                        String scoreText = getScoreText();
                        if (!scoreText.equals(scoreLabel.getText())) {
                            scoreLabel.setText(scoreText);
                        }
                        String timerText = getTotalTime();
                        if (!timerText.equals(timerLabel.getText())) {
                            timerLabel.setText(timerText);
                        }
                        repaint();
                        if(colorBox.isAllBoxDisapeared())
                        {
                            App.nexLevelMove();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(AppConfiguration.GAME_SPEED_NEGATIVE);
                    } catch (InterruptedException ie) {
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

	private String getTotalTime()
	{
		long seconds = (new Date().getTime() - this.gameDateTimeValue.getTime())/1000;
		String diffText = String.format("%02d" , (seconds / 60)) + ":" + String.format("%02d" , (seconds % 60));
		return "Time: " + diffText;
	}

    private String getScoreText()
	{
		return "Score: " + colorBox.getScore();
	}

    protected void paintComponent(Graphics g) {
        if (colorBox != null) {
            super.paintComponent(g);
            colorBox.drawcolorbox(g);
        }
    }

    public void stopThread() {
        colorBox.runEndless = false;
        System.out.println("End thread");
    }
}
