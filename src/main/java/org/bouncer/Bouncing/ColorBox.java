package org.bouncer.Bouncing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class ColorBox {

	public static double border_xpos = 100, border_ypos = 50, border_width = 1200, border_height = 620;
	public static double boxwidth = 100, boxheight = 50;
	public double ix = 570, iw = 100, ih = 20, iy = border_ypos + border_height - 2 * ih;
	int[][] rectcheck = new int[10][4];
	Rectangle2D.Double border = new Rectangle2D.Double(border_xpos, border_ypos, border_width, border_height);
	Rectangle2D.Double[][] rect;
	double[][][][] walist;
	double[][][] wallborder = new double[2][4][2];
	int i, j, k;
	private int score;
	public boolean runEndless = true;
	BouncingBall ball = new BouncingBall();
	int horizontalLength, verticalLength;
	int gameLevel;

	public ColorBox(Integer gameLevel, Integer score) {
		this.score = score;
		this.gameLevel = gameLevel;
		horizontalLength = AppConfiguration.GAME_LEVEL_WALL_MAP.get(gameLevel)[0];
		verticalLength = AppConfiguration.GAME_LEVEL_WALL_MAP.get(gameLevel)[1];
		walist = new double[horizontalLength][verticalLength][4][2];
		rect = new Rectangle2D.Double[horizontalLength][verticalLength];
		ball.initPosition(ix, iw, iy);

		for (i = 0; i < horizontalLength; i++) {
			for (j = 0; j < verticalLength; j++) {
				rect[i][j] = new Rectangle2D.Double(border_xpos + 50 + i * 110, border_ypos + 50 + j * 100, boxwidth,
						boxheight);
				rectcheck[i][j] = 1;
			}
		}

		for (i = 0; i < horizontalLength; i++) {
			for (j = 0; j < verticalLength; j++) {

				walist[i][j][0][0] = rect[i][j].getX();
				walist[i][j][0][1] = rect[i][j].getY();
				walist[i][j][1][0] = rect[i][j].getX() + boxwidth;
				walist[i][j][1][1] = rect[i][j].getY();
				walist[i][j][2][0] = rect[i][j].getX() + boxwidth;
				walist[i][j][2][1] = rect[i][j].getY() + boxheight;
				walist[i][j][3][0] = rect[i][j].getX();
				walist[i][j][3][1] = rect[i][j].getY() + boxheight;

			}
		}

		wallborder[0][0][0] = border_xpos;
		wallborder[0][0][1] = border_ypos;
		wallborder[0][1][0] = border_xpos + border_width;
		wallborder[0][1][1] = border_ypos;
		wallborder[0][2][0] = border_xpos + border_width;
		wallborder[0][2][1] = border_ypos + border_height;
		wallborder[0][3][0] = border_xpos;
		wallborder[0][3][1] = border_ypos + border_height;
		wallborder[1][0][0] = ix;
		wallborder[1][0][1] = iy;
		wallborder[1][1][0] = ix + iw;
		wallborder[1][1][1] = iy;
		wallborder[1][2][0] = ix + iw;
		wallborder[1][2][1] = iy + ih;
		wallborder[1][3][0] = ix;
		wallborder[1][3][1] = iy + ih;

	}

	public void drawcolorbox(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		for (i = 0; i < 10; i++) {
			for (j = 0; j < 4; j++) {
				if (rectcheck[i][j] == 1) {
					g2.draw(rect[i][j]);
				}
			}
		}
		g2.draw(border);
		g2.setPaint(Color.BLUE);
		g.fillOval((int) (ix), (int) (iy), (int) iw, (int) ih);
		ball.drawball(g);
	}

	@SuppressWarnings({"UnusedAssignment"})
	public synchronized void rearrangeBorder() {
		double dist, disa, disb, dis = 0, disab;

		// check border
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 4; j++) {
				disab = Math.sqrt((wallborder[i][(j + 1) % 4][1] - wallborder[i][j][1])
						* (wallborder[i][(j + 1) % 4][1] - wallborder[i][j][1])
						+ (wallborder[i][(j + 1) % 4][0] - wallborder[i][j][0])
								* (wallborder[i][(j + 1) % 4][0] - wallborder[i][j][0]));
				dist = ((wallborder[i][(j + 1) % 4][1] - wallborder[i][j][1]) * ball.xpos
						- (wallborder[i][(j + 1) % 4][0] - wallborder[i][j][0]) * ball.ypos
						+ wallborder[i][(j + 1) % 4][0] * wallborder[i][j][1]
						- wallborder[i][(j + 1) % 4][1] * wallborder[i][j][0]) / disab;
				disa = Math.sqrt((wallborder[i][j][0] - ball.xpos) * (wallborder[i][j][0] - ball.xpos)
						+ (wallborder[i][j][1] - ball.ypos) * (wallborder[i][j][1] - ball.ypos));
				disb = Math.sqrt((wallborder[i][(j + 1) % 4][0] - ball.xpos)
						* (wallborder[i][(j + 1) % 4][0] - ball.xpos)
						+ (wallborder[i][(j + 1) % 4][1] - ball.ypos) * (wallborder[i][(j + 1) % 4][1] - ball.ypos));
				if (dist < 0)
					dis = dis * (-1);
				if (disa < 0)
					disa = disa * (-1);
				if (disb < 0)
					disb = disb * (-1);

				if (disa * disa + disab * disab < disb * disb) {
					dis = disa;
				} else if (disb * disb + disab * disab < disa * disa) {
					dis = disb;
				} else
					dis = dist;

				if (dis < BouncingBall.ballsize && dis > -BouncingBall.ballsize) {
					if(i == 0 && j == 2)
					{
						App.gameEnd();
					}
					if (ball.changedirection(1000, i, j)) {
						disappearbox(1000, i);
					}
					ball.directionchangelock = new int[] { 1000, i, j };
				}
			}

		}

		// wall check
		for (i = 0; i < this.horizontalLength; i++) {
			for (j = 0; j < this.verticalLength; j++) {
				for (k = 0; k < 4; k++) {
					if (rectcheck[i][j] != 0) {
						disab = Math.sqrt((walist[i][j][(k + 1) % 4][1] - walist[i][j][k][1])
								* (walist[i][j][(k + 1) % 4][1] - walist[i][j][k][1])
								+ (walist[i][j][(k + 1) % 4][0] - walist[i][j][k][0])
										* (walist[i][j][(k + 1) % 4][0] - walist[i][j][k][0]));
						dist = ((walist[i][j][(k + 1) % 4][1] - walist[i][j][k][1]) * ball.xpos
								- (walist[i][j][(k + 1) % 4][0] - walist[i][j][k][0]) * ball.ypos
								+ walist[i][j][(k + 1) % 4][0] * walist[i][j][k][1]
								- walist[i][j][(k + 1) % 4][1] * walist[i][j][k][0]) / disab;
						disa = Math.sqrt((walist[i][j][k][0] - ball.xpos) * (walist[i][j][k][0] - ball.xpos)
								+ (walist[i][j][k][1] - ball.ypos) * (walist[i][j][k][1] - ball.ypos));
						disb = Math.sqrt(
								(walist[i][j][(k + 1) % 4][0] - ball.xpos) * (walist[i][j][(k + 1) % 4][0] - ball.xpos)
										+ (walist[i][j][(k + 1) % 4][1] - ball.ypos)
												* (walist[i][j][(k + 1) % 4][1] - ball.ypos));
						if (dist < 0)
							dis = dis * (-1);
						if (disa < 0)
							disa = disa * (-1);
						if (disb < 0)
							disb = disb * (-1);

						if (disa * disa + disab * disab < disb * disb) {
							dis = disa;
						} else if (disb * disb + disab * disab < disa * disa) {
							dis = disb;
						} else
							dis = dist;

						if (dis < BouncingBall.ballsize && dis > -BouncingBall.ballsize) {
							if (ball.changedirection(i, j, k)) {
								disappearbox(i, j);
							}
							ball.directionchangelock = new int[] { i, j, k };
						}
					}
				}
			}
		}
	}

	public void moveBall() {
		ball.move();
	}

	public void rearrangeWall() {
		wallborder[1][0][0] = ix;
		wallborder[1][0][1] = iy;
		wallborder[1][1][0] = ix + iw;
		wallborder[1][1][1] = iy;
		wallborder[1][2][0] = ix + iw;
		wallborder[1][2][1] = iy + ih;
		wallborder[1][3][0] = ix;
		wallborder[1][3][1] = iy + ih;
	}

	public void disappearbox(int i, int j) {
		rectcheck[i][j] = 0;
		incrementScore();
	}

	public boolean isAllBoxDisapeared()
	{
		return countDisapeardBox() >= (this.horizontalLength * this.verticalLength);
	}

	public int countDisapeardBox()
	{
		int count = 0;
		for (i = 0; i < this.horizontalLength; i++) {
			for (j = 0; j < this.verticalLength; j++) {
				if(rectcheck[i][j] == 0)
				{
					count++;
				}
			}
		}
		return count;
	}

	private void incrementScore() {
		score++;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
