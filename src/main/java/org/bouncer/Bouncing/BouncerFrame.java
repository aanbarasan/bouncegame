package org.bouncer.Bouncing;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
public class BouncerFrame extends JFrame {
	
	ColorBox colorBox;
	BouncerPanel bouncerPanel;
	PadKeyListener padKeyListener;

	public BouncerFrame(Integer level, int score, Date gameDateTime) {
		colorBox = new ColorBox(level, score);
		bouncerPanel = new BouncerPanel(colorBox, gameDateTime);
		padKeyListener = new PadKeyListener(colorBox);
		
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		double tem = 1.500000;
		int width = (int) ((double) screenSize.width / tem);
		int height = (int) ((double) screenSize.height / tem);
		setSize(width, height);
		setLocation(300, 150);
		setTitle("Bouncer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setResizable(true);
		add(bouncerPanel);
		addKeyListener(padKeyListener);
	}

	public void stopThread()
	{
		bouncerPanel.stopThread();
	}

	public void distroyComponent() {
		bouncerPanel.stopThread();
		this.dispose();
	}

	public int getScore() {
		return colorBox.getScore();
	}
}
