package org.bouncer.Bouncing;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PadKeyListener implements KeyListener {

    ColorBox colorbox;

    public PadKeyListener(ColorBox col) {
        colorbox = col;
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {

        if (colorbox.runEndless) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                if (colorbox.ix > ColorBox.border_xpos + ColorBox.border_width - colorbox.iw - 10) {
                    colorbox.ix = ColorBox.border_xpos + ColorBox.border_width - colorbox.iw;
                } else
                    colorbox.ix = colorbox.ix + 10;

                colorbox.rearrangeWall();
                colorbox.rearrangeBorder();
            }
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                if (colorbox.ix < ColorBox.border_xpos + 10) {
                    colorbox.ix = ColorBox.border_xpos;
                } else {
                    colorbox.ix = colorbox.ix - 10;
                }
                colorbox.rearrangeWall();
                colorbox.rearrangeBorder();
            }
        }

    }

    public void keyReleased(KeyEvent e) {
    }
}
